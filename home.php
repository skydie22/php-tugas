<?php 
session_start();
include 'koneksi.php';

$username = $_SESSION['username'];

$sql = "SELECT * FROM users";
$result = mysqli_query($koneksi, $sql);

//fetch all
$hasil = mysqli_fetch_all($result, MYSQLI_ASSOC);

	 if ($_SESSION["login"] !== 1) {
		header("Location: index.php");

		exit;
	}

    if(isset($_GET['pesan'])){
        if($_GET['pesan'] === 'berhasil_ubah'){
            $pesan = "Berhasil Mengubah Data";
        }
        if($_GET['pesan'] === 'berhasil_hapus'){
            $pesan = "Berhasil Menghapus Data";
        }
    }

 ?>



<!DOCTYPE html>
<html lang="en">
<head>
 	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My Blog</title>

</head>
  <style>

        * {
            font-family: roboto;

        }

        .container {
            font-size: 15px;
        }
        .contrainer {
        	background: url("home.png") no-repeat; background-size: cover;
	     width: 100%;
        }
    </style>
</head>

<body>

     <!-- Navbar -->
     <div class="contrainer">
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" style="background-color: #fff">
        <div class="container">
            <a class="navbar-brand " href="home.php" style="font-weight: bold; color: #1a1a1a">
                <i style="font-size: 23px; color: rgb(245, 245, 245);" ></i>
                Website</a>
                <a href="registrasi.php" class="btn btn-primary" >Tambah Data</a>
                <div class="ml-auto navbar-nav">
                    <a type="button"style="width:110px;" class="btn btn-primary " href="logout.php">Log out</a>
                </div>
        </div>
    </nav>
    <!-- Navbar End-->

 <div class="container" style="margin-top: 200px;">
    <h1>Table Users</h1>
    <?php
    if(isset($_GET['pesan'])){
        ?>
    <div class="alert alert-success" role="alert">
      <?=$pesan;?>
    </div>
    <?php
}
?>
                  
    <table class="table table-hover">
        <thead>
            <tr>
                <th>No.</th>
                <th>Nama</th>
                <th>username</th>
                <th>Email</th>
                <th>Avatar</th>
                <th>Action</th>
            </tr>
        </thead>

        <tbody>
            <?php
            foreach ($hasil as $key => $user_data) {
                ?>
                <tr>
                    <td><?= $key + 1 ?></td>
                    <td><?= $user_data["nama"] ?></td>
                    <td><?= $user_data["username"] ?></td>
                    <td><?= $user_data["email"] ?></td>
                    <td><?= $user_data["avatar"] ?></td>
                    <td>
                        <a href="form_ubah.php?id=<?=$user_data['id']?>" class="btn btn-primary " >ubah</a>

                        <a href="proses_hapus.php?id=<?=$user_data['id']?>" class="btn btn-danger" >hapus</a>

                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
        <tfoot>
            
        </tfoot>
    </table>



    </div>
 </div>

</body>

</html>
